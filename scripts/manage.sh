#!/usr/bin/env bash

DISABLE_PROGRESS=false
OUTPUT_DEVICE=/dev/stdout
if [ "$DISABLE_PROGRESS" == true ]; then
    OUTPUT_DEVICE=/dev/null;
fi

CONFIG_FILE=config.ini
CREATE_DEFAULT_CONFIG=false
SERVER_VERSION=
CONFIG_FILE=
SAVEGAME=
ACTION=

SERVER_SETTINGS=
MAP_GEN_SETTINGS=
MAP_SETTINGS=

SERVER_DIR=
CONFIG_DIR=
SAVE_DIR=
MOD_DIR=
TMP_DIR=tmp-factorio

SCRIPT_PATH=$(dirname $0)


#
# Function definitions
#

output_on () {
    ! [ "$DISABLE_PROGRESS" == true ]
}

usage () {
    echo "Usage: $(basename $0) -c|--config <config-file> [-d|--default] [-a|--action download|select] [-s|--save <save>] [-m|--modpack <mod-pack>] [-v|--version <version>] [--server-settings <config-file>]"
}

create_default_config () {
    if [ "$CREATE_DEFAULT_CONFIG" == false ]; then
        return;
    fi

    CONFIG_FILE_DIR=$(realpath $(dirname $CONFIG_FILE))

    echo "server_dir=$CONFIG_FILE_DIR/server"   > $CONFIG_FILE
    echo "config_dir=$CONFIG_FILE_DIR/config"   >> $CONFIG_FILE
    echo "save_dir=$CONFIG_FILE_DIR/saves"      >> $CONFIG_FILE
    echo "mod_dir=$CONFIG_FILE_DIR/mods"        >> $CONFIG_FILE
    echo "tmp_dir=$CONFIG_FILE_DIR/tmp"         >> $CONFIG_FILE

    output_on && echo "Default configuration '$CONFIG_FILE' created"
}

parse_config () {

    # Known config keys:
    # server_dir
    # config_dir
    # save_dir
    # mod_dir

    if ! [ -f "$CONFIG_FILE" ]; then
        echo "Config file '$CONFIG_FILE' does not exist!"
        exit 1
    fi

    SERVER_DIR_tmp=$(grep 'server_dir *=' $1 | sed -r 's/^\s*server_dir *= *(.*)$/\1/g')
    if [ -n $SERVER_DIR_tmp ]; then
        SERVER_DIR=$SERVER_DIR_tmp
    fi
    if [ ! -d $SERVER_DIR ]; then
        echo "Server path '$SERVER_DIR' not found or no directory"
        exit 1;
    fi

    CONFIG_DIR_tmp=$(grep 'config_dir *=' $1 | sed -r 's/^\s*config_dir *= *(.*)$/\1/g')
    if [ -n $CONFIG_DIR_tmp ]; then
        CONFIG_DIR=$CONFIG_DIR_tmp
    fi
    if [ ! -d $CONFIG_DIR ]; then
        echo "Config path '$CONFIG_DIR' not found or no directory"
        exit 1;
    fi

    SAVE_DIR_tmp=$(grep 'save_dir *=' $1 | sed -r 's/^\s*save_dir *= *(.*)$/\1/g')
    if [ -n $SAVE_DIR_tmp ]; then
        SAVE_DIR=$SAVE_DIR_tmp
    fi
    if [ ! -d $SAVE_DIR ]; then
        echo "Save path '$SAVE_DIR' not found or no directory"
        exit 1;
    fi

    MOD_DIR_tmp=$(grep 'mod_dir *=' $1 | sed -r 's/^\s*mod_dir *= *(.*)$/\1/g')
    if [ -n $MOD_DIR_tmp ]; then
        MOD_DIR=$MOD_DIR_tmp
    fi
    if [ ! -d $MOD_DIR ]; then
        echo "Mod path '$MOD_DIR' not found or no directory"
        exit 1;
    fi

    TMP_DIR_tmp=$(grep 'tmp_dir *=' $1 | sed -r 's/^\s*tmp_dir *= *(.*)$/\1/g')
    if [ -n $TMP_DIR_tmp ]; then
        TMP_DIR=$TMP_DIR_tmp
    fi


    # Outputs
    if output_on; then
        echo "Configurations found:"
        echo "SERVER_DIR      : $SERVER_DIR"
        echo "CONFIG_DIR      : $CONFIG_DIR"
        echo "SAVE_DIR        : $SAVE_DIR"
        echo "MOD_DIR         : $MOD_DIR"
        echo "TMP_DIR         : $TMP_DIR"
        echo;
    fi

}

parse_current_configs () {

    if [ -f $CONFIG_DIR/current_version -a -z "$CURRENT_VERSION" ]; then
        UNRESOLVED_CURRENT_VERSION=$(cat $CONFIG_DIR/current_version)
        resolve_current_version $UNRESOLVED_CURRENT_VERSION
    fi

    if [ -f $CONFIG_DIR/current_save -a -z "$CURRENT_SAVE" ]; then
        CURRENT_SAVE=$(cat $CONFIG_DIR/current_save)
    fi

    if [ -f $CONFIG_DIR/current_mods -a -z "$CURRENT_MODS" ]; then
        CURRENT_MODS=$(cat $CONFIG_DIR/current_mods)
    fi

    if output_on; then
        echo "Currently selected configuration:"
        echo -n "CURRENT_VERSION : $UNRESOLVED_CURRENT_VERSION"
        if [ "$UNRESOLVED_CURRENT_VERSION" == "latest" ]; then
            echo " -> $CURRENT_VERSION";
        else
            echo;
        fi
        echo "CURRENT_SAVE    : $CURRENT_SAVE"
        echo "CURRENT_MODS    : $CURRENT_MODS"
        echo;
    fi
}

verify_options () {

    # action
    case "$ACTION" in
        "download"|"select"|"create_map") ;;
        "") echo "No action specified!"      ; exit 1 ;;
        *)  echo "Invalid action '$ACTION'!" ; exit 1 ;;
    esac

    # version
    if [ -n "$SERVER_VERSION" ]; then
        VERSION_REGEX=^[0-9]+\.[0-9]+\.[0-9]+$
        if ! [[ "$SERVER_VERSION" =~ "$VERSION_REGEX" ]] && ! [ "$SERVER_VERSION" == "latest" ]; then
            echo "Invalid version string '$SERVER_VERSION'!"
            exit 1;
        fi
    fi

    # save game
    if [ -n "$SAVEGAME" ]; then
        if [ ! -f $SAVE_DIR/$SAVEGAME/$SAVEGAME.zip ]; then
            output_on && echo "Savegame '$SAVEGAME' does not exist yet";
        fi
    fi

    # modpack
    if [ -n "$MODPACK" ]; then
        if [ ! -d $MOD_DIR/$MODPACK -a "${MODPACK^h}" != "none" ]; then
            echo "Modpack path '$MODPACK' not found or no directory!"
            exit 1;
        fi
    fi

}


download () {
    output_on && echo "Download server executables..."

    if [ -z $SERVER_VERSION ]; then
        output_on && echo "No server version specified. Falling back to 'latest'"
        SERVER_VERSION=latest;
    fi

    SILENT_CURL=
    if [ "$DISABLE_PROGRESS" == true ]; then
        SILENT_CURL=" --silent "
    fi

    # create temporary directory
    rm -rf $TMP_DIR
    mkdir -p $TMP_DIR

    # resolve download URL to an effective file URL
    SERVER_URL=https://factorio.com/get-download/$SERVER_VERSION/headless/linux64
    CURL_RESULT=$(curl $SILENT_CURL -L -o $TMP_DIR/server -w "%{http_code} %{url_effective}" $SERVER_URL)
    IFS=" " read STATUS_CODE EFFECTIVE_URL <<< $CURL_RESULT

    if output_on; then
        echo "Download link           : $SERVER_URL"
        echo "HTTP status code        : $STATUS_CODE";
    fi

    # verify that the HTTP request returned with success (200 - 299)
    if ! [ "$STATUS_CODE" -ge 200 -a "$STATUS_CODE" -lt 300 ]; then
        echo "Error while downloading server: Unexpected status code '$STATUS_CODE'!"
        exit 1;
    fi

    # query the actual version from the filename
    FILENAME=$(echo $(basename $EFFECTIVE_URL) | sed -r 's/([^?]+)\?.*/\1/g')
    VERSION=$(echo $FILENAME | sed -r 's/.*?_([0-9.]+)\.tar\.xz/\1/g')

    output_on && echo "Server version found    : $VERSION"

    # if this version is already installed abort
    if [ -d $SERVER_DIR/$VERSION ]; then
        output_on && echo "Version $VERSION is already installed!"
        exit 0
    fi

    mv $TMP_DIR/server $TMP_DIR/$FILENAME

    if [ "$?" != "0" ]; then
        echo "Failed to download server!"
        rm -f $TMP_DIR
        exit 1;
    fi

    cd $TMP_DIR
    # decompress and extract the archive
    output_on && echo "Decompress..."
    xz -d $FILENAME

    FILENAME=${FILENAME/.xz/} || { rm -rf $TMP_DIR; exit 1; }

    output_on && echo "Extract archive..."
    tar -xf $FILENAME || { rm -rf $TMP_DIR; exit 1; }
    cd - &> /dev/null

    # Care: factorio is the directory name from WITHIN the tar archive!
    output_on && echo "Move unpacked server..."
    mv $TMP_DIR/factorio $SERVER_DIR/$VERSION || { rm -rf $TMP_DIR; exit 1; }

    output_on && echo "Server files with version '$SERVER_VERSION' are ready!"

    # cleanup temporary directory
    rm -rf $TMP_DIR

}


configure () {

    local ADHOC=true
    if [ "$ACTION" == "select" ]; then
        ADHOC=false
    fi
    local SELECTED=false

    # if this version is not already installed abort
    if [ -n "$SERVER_VERSION" ]; then
        resolve_current_version $SERVER_VERSION

        if [ "$ADHOC" == false ]; then
            if [ ! -d $SERVER_DIR/$CURRENT_VERSION ]; then
                echo "Version '$CURRENT_VERSION' is not installed!"
                exit 1;
            fi

            echo $SERVER_VERSION > $CONFIG_DIR/current_version
            output_on && echo "Version '$SERVER_VERSION' selected!"

            SELECTED=true;
        else
            UNRESOLVED_CURRENT_VERSION=$SERVER_VERSION;
        fi
    fi

    if [ -n "$SAVEGAME" ]; then
        if [ "$ADHOC" == false ]; then
            echo $SAVEGAME > $CONFIG_DIR/current_save
            output_on && echo "Save '$SAVEGAME' selected!"

            SELECTED=true;
        else
            CURRENT_SAVE=$SAVEGAME;
        fi
    fi

    if [ -n "$MODPACK" ]; then
        if [ "$ADHOC" == false ]; then
            # variable MODPACK got already verified in verify_options!
            echo $MODPACK > $CONFIG_DIR/current_mods
            output_on && echo "Modpack '$MODPACK' selected!"

            SELECTED=true;
        else
            CURRENT_MODS=$MODPACK;
        fi
    fi

    if [ "$SELECTED" == "false" -a "$ADHOC" == false ]; then
        output_on && echo "No selection was changed!";
    fi

}

resolve_current_version () {
    if [ "$1" == "latest" ]; then
        CURRENT_VERSION=$(ls $SERVER_DIR | sort | tail -n 1)
        if [ -n "$CURRENT_VERSION" ]; then
            output_on && echo "Resolved server version 'latest' to '$CURRENT_VERSION'";
        else
            CURRENT_VERSION=$1;
        fi
    else
        CURRENT_VERSION=$1;
    fi
}

create_map () {
    #
    # Verify variables
    #

    # do not create a map without version or save name set
    if [ -z "$CURRENT_VERSION" ]; then
        echo "No server version has been selected yet!"
        exit 1;
    fi
    if [ -z "$CURRENT_SAVE" ]; then
        echo "No save game has been selected yet!"
        exit 1;
    fi

    # server config
    if [ -n "$SERVER_SETTINGS" -a ! -f "$SERVER_SETTINGS" ]; then
        echo "Server settings '$SERVER_SETTINGS' not found or not a file!"
        exit 1;
    fi

    # map gen settings
    if [ -n "$MAP_GEN_SETTINGS" -a ! -f "$MAP_GEN_SETTINGS" ]; then
        echo "Map generation settings '$MAP_GEN_SETTINGS' not found or not a file!"
        exit 1;
    fi

    # map settings
    if [ -n "$MAP_SETTINGS" -a ! -f "$MAP_SETTINGS" ]; then
        echo "Map settings '$MAP_SETTINGS' not found or not a file!"
        exit 1;
    fi

    # do not override an existing save
    if [ -d $SAVE_DIR/$CURRENT_SAVE ]; then
        echo "Directory '$SAVE_DIR/$CURRENT_SAVE'! Please remove it or use another name for the save."
        exit 1;
    fi


    mkdir $SAVE_DIR/$CURRENT_SAVE

    EXECUTABLE="$SERVER_DIR/$CURRENT_VERSION/bin/x64/factorio"
    PARAMETERS="--create $SAVE_DIR/$CURRENT_SAVE/$CURRENT_SAVE.zip"

    # set mod directory
    if [ -n "$CURRENT_MODS" -a "${CURRENT_MODS^h}" != "none" ]; then
        PARAMETERS+=" --mod-directory '$CURRENT_MODS'";
    fi

    # set server settings
    if [ -z "$SERVER_SETTINGS" ]; then
        SERVER_SETTINGS=$SERVER_DIR/$CURRENT_VERSION/data/server-settings.example.json
        output_on && echo "No server settings specified! Falling back to example settings.";
    fi
    cp "$SERVER_SETTINGS" "$SAVE_DIR/$CURRENT_SAVE/server-settings.json"
    PARAMETERS+=" --server-settings $SAVE_DIR/$CURRENT_DIR/server-settings.json";

    # set map generation settings
    if [ -z "$MAP_GEN_SETTINGS" ]; then
        MAP_GEN_SETTINGS=$SERVER_DIR/$CURRENT_VERSION/data/map-gen-settings.example.json
        output_on && echo "No map generation settings specified! Falling back to example settings.";
    fi
    cp "$MAP_GEN_SETTINGS" "$SAVE_DIR/$CURRENT_SAVE/map-gen-settings.json"
    PARAMETERS+=" --map-gen-settings $SAVE_DIR/$CURRENT_SAVE/map-gen-settings.json"

    # set map settings
    if [ -z "$MAP_SETTINGS" ]; then
        MAP_SETTINGS=$SERVER_DIR/$CURRENT_VERSION/data/map-settings.example.json
        output_on && echo "No map settings specified! Falling back to example settings.";
    fi
    cp "$MAP_SETTINGS" "$SAVE_DIR/$CURRENT_SAVE/map-settings.json"
    PARAMETERS+=" --map-settings $SAVE_DIR/$CURRENT_SAVE/map-settings.json";

    # create map
    output_on && echo "Execute: $EXECUTABLE $PARAMETERS"
    $EXECUTABLE $PARAMETERS > $OUTPUT_DEVICE

}


#
# Parse arguments
#

TEMP=`getopt -o hdc:s:m:v:a:p --long help,default,config:,save:,modpack:,version:,action:,preview,server-settings:,map-gen-settings:,map-settings: -- "$@"`
RES=$?; [ "$RES" != "0" ] && exit $RES
eval set -- "$TEMP"


while true ; do
    case "$1" in
        -h|--help)          usage                       ; exit 0 ;;
        -c|--config)        CONFIG_FILE=$2              ; shift 2 ;;
        -d|--default)       CREATE_DEFAULT_CONFIG=true  ; shift 1 ;;

        -v|--version)       SERVER_VERSION=$2           ; shift 2 ;;
        -s|--save)          SAVEGAME=$2                 ; shift 2 ;;
        -m|--modpack)       MODPACK=$2                  ; shift 2 ;;
        -a|--action)        ACTION=$2                   ; shift 2 ;;
        -p|--preview)       PREVIEW=true                ; shift 1 ;;

        --server-settings)  SERVER_SETTINGS=$2          ; shift 2 ;;
        --map-gen-settings) MAP_GEN_SETTINGS=$2         ; shift 2 ;;
        --map-settings)     MAP_SETTINGS=$2             ; shift 2 ;;

        --) break ;;
        *)  echo "Internal error!"; exit 1 ;;
    esac
done

if [ -z "$CONFIG_FILE" ]; then
    usage
    exit 1;
fi


if [ "$CREATE_DEFAULT_CONFIG" == true ]; then
    create_default_config
    parse_config $CONFIG_FILE
    exit 0;
fi
parse_config $CONFIG_FILE
verify_options


# even if the action is not 'select' allow adhoc configurations
configure
if [ "$ACTION" == "select" ]; then
    # if the action was to select a configuration, the job is done here
    exit 0;
fi

parse_current_configs

#
# Execute selected action
#

case "$ACTION" in
    "download")   download   ;;
    "select")     : ;;
    "create_map") create_map ;;
    *) echo "Internal error!"; exit 1 ;;
esac

# $SERVER_DIR/$CURRENT_VERSION/bin/x64/factorio --mod-directory $MOD_DIR --server-settings $CONFIG_DIR/$SERVER_CONFIG.json --create $SAVE_DIR/$CURRENT_SAVE.zip
# $SERVER_DIR/$CURRENT_VERSION/bin/x64/factorio --mod-directory $MOD_DIR --server-settings $CONFIG_DIR/$SERVER_CONFIG.json --map-gen-seed <> --generate-map-preview $TMP_DIR/$CURRENT_SAVE.zip
