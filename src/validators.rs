use std::str::FromStr;
use crate::utils::ServerVersion;


pub fn valid_version(arg: String) -> Result<(), String> {
    ServerVersion::from_str(&arg)?;
    Ok(())
}

pub fn existing_file(arg: String) -> Result<(), String> {
    if std::path::Path::new(&arg).is_file() {
        Ok(())
    } else {
        Err("File not found or a directory".to_owned())
    }
}

pub fn non_empty(arg: String) -> Result<(), String> {
    if arg.len() == 0 {
        Err("Argument must not be empty".to_owned())
    } else {
        Ok(())
    }
}
