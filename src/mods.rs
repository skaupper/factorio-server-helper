#![allow(dead_code)]

use reqwest::Client;
use std::path::Path;
use std::str::FromStr;
use crate::utils::ServerVersion;


pub struct ModClient {
    client: Client,
    token: String,
    username: String
}


impl ModClient {
    pub fn login(username: &str, password: &str) -> ModClient {
        static LOGIN_URL: &str = "https://auth.factorio.com/api-login";

        let client = reqwest::Client::builder()
            .cookie_store(true)
            .build()
            .unwrap();

        // build the login request
        let parameters = [
            ("username", username),
            ("password", password)
        ];

        // request the token using username and password
        let response = client.post(LOGIN_URL)
            .form(&parameters)
            .send()
            .unwrap()
            .text()
            .unwrap();

        let mut token_json = json::parse(&response).unwrap()[0].take();
        let token = token_json.take_string().unwrap();

        ModClient {
            client,
            token,
            username: username.to_owned()
        }
    }

    pub fn download_mods_from_list(&self, mod_pack_path: &Path, modlist_path: &Path) {
        let modlist = std::fs::read_to_string(modlist_path).unwrap();
        let modlist_json = json::parse(&modlist).unwrap();

        modlist_json["mods"].members()
            .filter(|e| {
                let enabled = e["enabled"].as_bool().unwrap();
                let is_base = e["name"].as_str().unwrap() == "base";

                enabled && !is_base
            })
            .map(|e| e["name"].as_str().unwrap())
            .for_each(|e| self.download_mod(mod_pack_path, e));
    }

    pub fn download_mod(&self, mod_pack_path: &Path, module: &str) {
        static MOD_URL_PREFIX: &str = "https://mods.factorio.com";

        let query_parameters = &[
            ("username", &self.username),
            ("token", &self.token)
        ];

        println!("Installing mod {}...", module);

        // request mod information
        let request_url = format!("{}/api/mods/{}", MOD_URL_PREFIX, module);
        let mod_info_response = self.client
            .get(&request_url)
            .query(query_parameters)
            .send()
            .unwrap()
            .text()
            .unwrap();


        // extract the download URL and the file name of the latest release
        let mod_info_json = json::parse(&mod_info_response).unwrap();
        let latest_release = mod_info_json["releases"].members()
            .max_by(|e1, e2| {
                let s1 = ServerVersion::from_str(e1["version"].as_str().unwrap()).unwrap();
                let s2 = ServerVersion::from_str(e2["version"].as_str().unwrap()).unwrap();
                s1.partial_cmp(&s2).unwrap_or(std::cmp::Ordering::Equal)
            })
            .unwrap();

        let file_name = latest_release["file_name"].as_str().unwrap();
        let download_url = latest_release["download_url"].as_str().unwrap();


        // download mod
        let mut response = self.client
            .get(&format!("{}/{}", MOD_URL_PREFIX, download_url))
            .query(query_parameters)
            .send()
            .unwrap();

        let file_path = mod_pack_path.join(file_name);
        let mut out = std::fs::File::create(&file_path).unwrap();
        std::io::copy(&mut response, &mut out).unwrap();
    }

    pub fn update_mods(&self, mod_pack_path: &Path) {
        // get all installed mods + mod names + mod versions
        let mod_infos: Vec<_> = mod_pack_path.read_dir().unwrap().filter_map(|res_dir_entry| {
            let dir_entry = res_dir_entry.unwrap();
            let os_file_name = dir_entry.file_name();
            let file_name = os_file_name.to_str().unwrap();

            // ignore mod-list.json and mod-settings.dat (and other non-zip files)
            if !file_name.ends_with(".zip") {
                return None;
            }

            // each mod file has a name of the format MODNAME_VERSION.zip
            let mod_infos: Vec<_> = file_name
                .trim_end_matches(".zip")
                .rsplitn(2, '_')
                .collect();

            Some((dir_entry.path(), mod_infos[1].to_string(), mod_infos[0].to_string()))
        }).collect();

        // do a fresh download of each mod
        for (path, name, _version) in mod_infos {
            // instead of deleting the old version make a backup just in case
            // if the download method fails, the backup could be used again
            let mut backup_path = path.clone();
            backup_path.set_extension("zip.bkp");

            std::fs::rename(path, &backup_path).unwrap();
            self.download_mod(mod_pack_path, &name);
            std::fs::remove_file(backup_path).unwrap();
        }
    }
}
