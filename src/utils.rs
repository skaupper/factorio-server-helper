use std::path::{Path, PathBuf};
use std::fmt;
use std::cmp::Ordering;
use std::str::FromStr;
use ini::Ini;
use crate::mods::ModClient;


#[derive(Default, Debug)]
pub struct ServerConfig {
    pub server_dir: PathBuf,
    pub save_dir: PathBuf,
    pub mod_dir: PathBuf,
    pub username: String,
    pub password: String
}

#[derive(Debug, PartialEq)]
pub enum ServerVersion {
    Latest,
    Specific(u16, u16, u16)
}



impl ServerConfig {
    pub fn create_from_file(config_file: &str) -> ServerConfig {
        let ini = Ini::load_from_file(config_file).unwrap();
        let properties = ini.general_section();

        let mut config: ServerConfig = Default::default();

        config.server_dir = properties.get("server_dir").map(|s| Path::new(s).into()).unwrap();
        config.save_dir = properties.get("save_dir").map(|s| Path::new(s).into()).unwrap();
        config.mod_dir = properties.get("mod_dir").map(|s| Path::new(s).into()).unwrap();
        config.username = properties.get("username").expect("Username must not be empty").to_string();
        config.password = properties.get("password").expect("Password must not be empty").to_string();



        let directories: &[&PathBuf] = &[
            &config.server_dir,
            &config.save_dir,
            &config.mod_dir,
        ];

        for d in directories {
            if !d.exists() {
                panic!("Directory {} specified in config file does not exist!", d.display());
            }
        }

        // do a test login to see if the credentials are valid
        ModClient::login(&config.username, &config.password);

        config
    }

    pub fn save_to_file(&self, config_file: &str) {
        let mut ini = Ini::new();
        let mut section_setter = ini.with_section::<String>(None);

        section_setter
            .set("server_dir", self.server_dir.to_str().unwrap())
            .set("save_dir", self.save_dir.to_str().unwrap())
            .set("mod_dir", self.mod_dir.to_str().unwrap())
            .set("username", self.username.clone())
            .set("password", self.password.clone());

        ini.write_to_file(config_file).unwrap();
    }
}

impl FromStr for ServerVersion {
    type Err = String;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s == "latest" {
            return Ok(ServerVersion::Latest);
        }

        let segments: Vec<_> = s.split('.').collect();
        if segments.len() != 3 {
            return Err("A version string must always contain of 3 numbers".to_owned());
        }

        let major = segments[0].parse().unwrap();
        let minor = segments[1].parse().unwrap();
        let patch = segments[2].parse().unwrap();

        Ok(ServerVersion::Specific(major, minor, patch))
    }
}

impl ServerVersion {
    pub fn new(major: u16, minor: u16, patch: u16) -> ServerVersion {
        ServerVersion::Specific(major, minor, patch)
    }

    pub fn get_latest_installed_version(server_dir: &Path) -> ServerVersion {
        let latest = server_dir.read_dir().unwrap()
            .map(|e| {
                let file_name = e.unwrap().file_name().into_string().unwrap();
                ServerVersion::from_str(&file_name).unwrap()
            })
            .fold(ServerVersion::Latest, |old, new| if new.gt(&old) { new } else { old });

        if latest == ServerVersion::Latest {
            panic!("No server has been installed yet");
        }

        latest
    }
}

impl fmt::Display for ServerVersion {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            ServerVersion::Specific(major, minor, patch) => write!(f, "{}.{}.{}", major, minor, patch),
            ServerVersion::Latest                        => write!(f, "latest"),
        }
    }
}

impl PartialOrd for ServerVersion {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        if *self == ServerVersion::Latest && *other == ServerVersion::Latest {
            return None;
        }

        if let ServerVersion::Latest = self {
            return Some(Ordering::Less);
        } else if let ServerVersion::Latest = other {
            return Some(Ordering::Greater);
        }

        let (s_major, s_minor, s_patch) = match self {
            ServerVersion::Specific(ma, mi, pa) => (ma, mi, pa),
            _ => panic!("Unexpected enum variant")
        };
        let (o_major, o_minor, o_patch) = match other {
            ServerVersion::Specific(ma, mi, pa) => (ma, mi, pa),
            _ => panic!("Unexpected enum variant")
        };

        let cmp_major = s_major.cmp(o_major);
        let cmp_minor = s_minor.cmp(o_minor);
        let cmp_patch = s_patch.cmp(o_patch);

        Some(cmp_major.then(cmp_minor).then(cmp_patch))
    }
}
