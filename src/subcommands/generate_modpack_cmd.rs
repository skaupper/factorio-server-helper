use clap::ArgMatches;
use crate::utils::ServerConfig;
use crate::mods::ModClient;
use std::path::Path;


pub fn generate_modpack(matches: &ArgMatches, config_file: &str) {
    //
    // CONFIG
    //
    let cfg_file = ServerConfig::create_from_file(config_file);
    let cfg_mod_pack_name = matches.value_of("MOD_PACK").unwrap();
    let cfg_mod_list_opt = matches.value_of("MOD_LIST");
    let cfg_mod_names_opt = matches.values_of("MOD_NAME");


    //
    // CREATE MOD PACK DIRECTORY
    //
    let mod_pack_path = cfg_file.mod_dir.join(&cfg_mod_pack_name);
    if mod_pack_path.is_dir() {
        panic!("Modpack {} already exists", cfg_mod_pack_name);
    }
    std::fs::create_dir(&mod_pack_path).unwrap();


    //
    // DOWNLOAD MODS
    //
    let client = ModClient::login(&cfg_file.username, &cfg_file.password);

    if let Some(mod_list) = cfg_mod_list_opt {
        let mod_list_path = Path::new(mod_list);
        if !mod_list_path.is_file() {
            panic!("Modlist {} does not exist", mod_list);
        }
        client.download_mods_from_list(&mod_pack_path, &mod_list_path);
    }

    // download mods specified as arguments
    if let Some(mod_names) = cfg_mod_names_opt {
        mod_names.for_each(|e| client.download_mod(&mod_pack_path, e));
    }


    println!("Modpack {} installed successfully", cfg_mod_pack_name);
}
