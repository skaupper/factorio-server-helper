use std::process::Command;
use clap::ArgMatches;
use crate::utils::{ServerConfig, ServerVersion};


pub fn start(matches: &ArgMatches, config_file: &str) {
    //
    // CONFIG
    //
    let cfg_file = ServerConfig::create_from_file(config_file);
    let cfg_server_version = value_t!(matches, "SERVER_VERSION", ServerVersion).unwrap();
    let cfg_save_name = matches.value_of("SAVE_NAME").unwrap();
    let cfg_mod_pack_opt = matches.value_of("MOD_PACK");


    //
    // LOCATE SERVER BINARY
    //
    let server_version_path = if let ServerVersion::Latest = cfg_server_version {
        cfg_file.server_dir.join(ServerVersion::get_latest_installed_version(&cfg_file.server_dir).to_string())
    } else {
        cfg_file.server_dir.join(cfg_server_version.to_string())
    };
    let server_binary_path = server_version_path.join("factorio/bin/x64/factorio");

    println!("Using server version {}", cfg_server_version);

    // check if files and directories exist
    if !server_version_path.is_dir() {
        panic!("Server version {} does not exist", cfg_server_version);
    }
    if !server_binary_path.is_file() {
        panic!("Server binary {} does not exist", server_binary_path.display());
    }


    //
    // LOCATE SAVE GAME
    //
    let save_path = cfg_file.save_dir.join(&cfg_save_name);
    let save_file_path = save_path.join(&format!("{}.zip", cfg_save_name));
    let server_settings_path = save_path.join("server-settings.json");

    println!("Using save game {}", cfg_save_name);

    // check if files and directories exist
    if !save_path.is_dir() {
        panic!("Savegame {} does not exist", save_path.display());
    }
    if !server_settings_path.is_file() {
        panic!("Server settings {} do not exist", server_settings_path.display());
    }
    if !save_file_path.is_file() {
        panic!("Map for savegame {} was not generated yet", cfg_save_name);
    }


    //
    // LOCATE MOD PACK
    //
    let mut chosen_mod_pack = None;

    // set mod directory if one was chosen (or default exists)
    if let Some(mod_pack_name) = cfg_mod_pack_opt {
        let mod_pack_path = cfg_file.mod_dir.join(mod_pack_name);

        // allow a default mod_pack with a few extra checks
        if !mod_pack_path.is_dir() && mod_pack_name != "default" {
            panic!("Mod pack {} does not exist", mod_pack_name);
        }

        if mod_pack_path.is_dir() {
            chosen_mod_pack = Some(mod_pack_path);
            println!("Using mod pack {}", mod_pack_name);
        }
    }


    //
    // START SERVER
    //
    let mut cmd = Command::new(server_binary_path);
    cmd.arg("--server-settings").arg(server_settings_path);
    cmd.arg("--start-server").arg(save_file_path);
    if let Some(mod_pack) = chosen_mod_pack {
        cmd.arg("--mod-directory").arg(mod_pack);
    }
    cmd.spawn().unwrap().wait().unwrap();
}
