use clap::ArgMatches;
use std::path::Path;
use crate::utils::ServerConfig;


pub fn init(matches: &ArgMatches, config_file: &str) {
    //
    // CONFIG
    //
    let cfg_root_dir = matches.value_of("ROOT_DIR").unwrap();
    let cfg_username = matches.value_of("USERNAME").unwrap();
    let cfg_password = matches.value_of("PASSWORD").unwrap();


    //
    // INITIALIZE ENVIRONMENT
    //
    let root_dir = shellexpand::full(cfg_root_dir).unwrap().into_owned();
    let root_path = Path::new(&root_dir);
    println!("Use root directory {}", root_path.display());


    // create server directories
    for dir in &["server", "saves", "mods"] {
        let path = root_path.join(dir);
        std::fs::create_dir_all(&path).unwrap();
        println!("Created path {}", path.display());
    }


    // create default config
    ServerConfig {
        server_dir: root_path.join("server"),
        save_dir:   root_path.join("saves"),
        mod_dir:    root_path.join("mods"),
        username:   cfg_username.to_owned(),
        password:   cfg_password.to_owned()
    }.save_to_file(config_file);


    println!("Created configuration file at {}", config_file);
}
