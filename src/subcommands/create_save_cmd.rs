use clap::ArgMatches;
use crate::utils::{ServerConfig, ServerVersion};


pub fn create_save(matches: &ArgMatches, config_file: &str) {
    //
    // CONFIG
    //
    let cfg_file = ServerConfig::create_from_file(config_file);
    let cfg_save_name = matches.value_of("SAVE_NAME").unwrap();
    let cfg_server_version = value_t!(matches, "SERVER_VERSION", ServerVersion).unwrap();


    //
    // LOCATE SERVER DIRECTORY
    //
    let version_path = if let ServerVersion::Latest = cfg_server_version {
        cfg_file.server_dir.join(ServerVersion::get_latest_installed_version(&cfg_file.server_dir).to_string())
    } else {
        cfg_file.server_dir.join(cfg_server_version.to_string())
    };

    if !version_path.is_dir() {
        panic!("Directory {} does not exist", version_path.display());
    }


    //
    // CREATE SAVE DIRECTORY
    //
    let save_path = cfg_file.save_dir.join(&cfg_save_name);
    if save_path.is_dir() {
        panic!("Save directory {} does already exist", save_path.display());
    }

    println!("Creating save path {} ...", save_path.display());
    std::fs::create_dir(&save_path).unwrap();


    //
    // COPY DEFAULT SETTINGS
    //
    let server_settings_path = version_path.join("factorio/data");
    for settings_file in &[
        "map-gen-settings.example.json",
        "map-settings.example.json",
        "server-settings.example.json"
    ] {
        println!("Copying default settings files {} ...", settings_file);
        let src = server_settings_path.join(&settings_file);
        let dest = save_path.join(&settings_file.replace(".example", ""));
        std::fs::copy(src, dest).unwrap();
    }


    println!("Save {} is ready to use", cfg_save_name);
}
