use clap::ArgMatches;
use crate::utils::ServerConfig;
use crate::mods::ModClient;


pub fn update_modpack(matches: &ArgMatches, config_file: &str) {
    //
    // CONFIG
    //
    let cfg_file = ServerConfig::create_from_file(config_file);
    let cfg_mod_pack_name = matches.value_of("MOD_PACK").unwrap();


    //
    // FIND MOD PACK DIRECTORY
    //
    let mod_pack_path = cfg_file.mod_dir.join(&cfg_mod_pack_name);
    if !mod_pack_path.is_dir() {
        panic!("Mod pack {} does not exist", cfg_mod_pack_name);
    }


    //
    // UPDATE MODS
    //
    let client = ModClient::login(&cfg_file.username, &cfg_file.password);
    client.update_mods(&mod_pack_path);


    println!("Mod pack {} updated successfully", cfg_mod_pack_name);
}
