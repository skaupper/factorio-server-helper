use clap::ArgMatches;
use crate::utils::{ServerConfig, ServerVersion};
use xz2::read::XzDecoder;
use tar::Archive;


pub fn download_server(matches: &ArgMatches, config_file: &str) {
    //
    // CONFIG
    //
    let cfg_file = ServerConfig::create_from_file(config_file);
    let cfg_server_version = value_t!(matches, "SERVER_VERSION", ServerVersion).unwrap();


    //
    // CHECK FOR ALREADY EXISTING SERVER FILES
    //      avoid downloads if possible
    //
    let version_path = cfg_file.server_dir.join(cfg_server_version.to_string());
    if version_path.is_dir() {
        panic!("Version {} already exists", cfg_server_version);
    }


    //
    // DOWNLOAD SERVER FILES
    //
    let server_url = format!("https://factorio.com/get-download/{}/headless/linux64", cfg_server_version.to_string());
    let response = reqwest::get(&server_url).unwrap();


    //
    // EXTRACT METAINFORMATION
    //      expects the file name to be of the format:
    //      factorio_headless_x64_<version>.tar.xz
    //
    let file_name = response.url()
        .path_segments()
        .unwrap()
        .last()
        .unwrap();
    let effective_version = file_name.trim_end_matches(".tar.xz")
        .split('_')
        .last()
        .unwrap()
        .to_owned();

    // check if the version does not already exist (important a version argument of 'latest')
    let effective_version_path = cfg_file.server_dir.join(&effective_version);
    if effective_version_path.is_dir() {
        panic!("Version {} already exists", &effective_version);
    }


    //
    // UNPACK DOWNLOADED ARCHIVE
    //
    let mut archive = Archive::new(XzDecoder::new(response));
    std::fs::create_dir(&effective_version_path).unwrap();
    archive.unpack(&effective_version_path).unwrap();


    println!("Server files for version {} downloaded successfully", &effective_version);
}
