use std::io::Write;
use clap::ArgMatches;
use crate::utils::ServerConfig;
use crate::mod_settings::deserialize_mod_settings;


pub fn dump_mod_settings(matches: &ArgMatches, config_file: &str) {
    //
    // CONFIG
    //
    let cfg_file = ServerConfig::create_from_file(config_file);
    let cfg_mod_pack_name = matches.value_of("MOD_PACK").unwrap();
    let cfg_output_file = matches.value_of("OUTPUT_FILE").unwrap();


    //
    // FIND MOD PACK DIRECTORY
    //
    let mod_pack_path = cfg_file.mod_dir.join(&cfg_mod_pack_name);
    let mod_settings_path = mod_pack_path.join("mod-settings.dat");
    if !mod_pack_path.is_dir() {
        panic!("Mod pack {} does not exist", cfg_mod_pack_name);
    }
    if !mod_settings_path.is_file() {
        panic!("Mod settings in {} not found", mod_pack_path.display());
    }


    //
    // DESERIALIZE MOD SETTINGS
    //
    let mut mod_settings = std::fs::File::open(&mod_settings_path).unwrap();
    let (version, property_tree) = deserialize_mod_settings(&mut mod_settings).unwrap();
    println!("Mod settings file for factorio version {} found", version);


    //
    // WRITE OUTPUT
    //
    let mut out: Box<dyn Write> = if cfg_output_file != "-" {
        Box::new(std::fs::File::create(cfg_output_file).unwrap())
    } else {
        Box::new(std::io::stdout())
    };

    property_tree.print_indented(&mut out, 0, 4);
}
