use std::process::Command;
use clap::ArgMatches;
use crate::utils::{ServerConfig, ServerVersion};


pub fn generate_map(matches: &ArgMatches, config_file: &str) {
    //
    // CONFIG
    //
    let cfg_file = ServerConfig::create_from_file(config_file);
    let cfg_save_name = matches.value_of("SAVE_NAME").unwrap();
    let cfg_server_version = value_t!(matches, "SERVER_VERSION", ServerVersion).unwrap();


    //
    // LOCATE SERVER BINARY
    //
    let version_path = if let ServerVersion::Latest = cfg_server_version {
        cfg_file.server_dir.join(ServerVersion::get_latest_installed_version(&cfg_file.server_dir).to_string())
    } else {
        cfg_file.server_dir.join(cfg_server_version.to_string())
    };
    let server_binary = version_path.join("factorio/bin/x64/factorio");

    if !server_binary.is_file() {
        panic!("Server binary {} does not exist", server_binary.display());
    }


    //
    // LOCATE SAVE FILE
    //
    let save_path = cfg_file.save_dir.join(&cfg_save_name);
    if !save_path.is_dir() {
        panic!("Savegame {} does not exist", save_path.display());
    }
    if save_path.join(&format!("{}.zip", cfg_save_name)).is_file() {
        panic!("Map for savegame {} does already exist", cfg_save_name);
    }


    //
    // INVOKE MAP GENERATION
    //
    // TODO: verify existence of map*-settings.json
    let mut cmd = Command::new(server_binary);
    cmd.arg("--map-gen-settings").arg(save_path.join("map-gen-settings.json"));
    cmd.arg("--map-settings").arg(save_path.join("map-settings.json"));
    cmd.arg("--create").arg(save_path.join(&format!("{}.zip", cfg_save_name)));
    cmd.output().unwrap();


    println!("Map for save {} got generated", save_path.display());
}
