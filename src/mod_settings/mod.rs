use std::io::{Read, Result};
use crate::utils::ServerVersion;


mod types;
mod deserialize;
mod serialize;

use deserialize::*;
pub use types::*;


pub fn deserialize_mod_settings<T: Read>(reader: &mut T) -> Result<(ServerVersion, PropertyTree)> {
    let version = deserialize_binary_version(reader)?;

    let always_false = deserialize_bool(reader)?;
    assert_eq!(always_false, false);

    let property_tree = deserialize_property_tree(reader)?;

    Ok((version, property_tree))
}
