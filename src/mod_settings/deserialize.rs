use std::io::{Read, Result};
use byteorder::{LittleEndian, ReadBytesExt};
use super::types::*;
use crate::utils::ServerVersion;


pub fn deserialize_byte<T: Read>(buf: &mut T) -> Result<u8> {
    buf.read_u8()
}

pub fn deserialize_bool<T: Read>(buf: &mut T) -> Result<bool> {
    let byte = buf.read_u8()?;
    Ok(byte == 1)
}

pub fn deserialize_number<T: Read>(buf: &mut T) -> Result<f64> {
    buf.read_f64::<LittleEndian>()
}

pub fn deserialize_uint<T: Read>(buf: &mut T) -> Result<u32> {
    buf.read_u32::<LittleEndian>()
}

pub fn deserialize_space_optimized_uint<T: Read>(buf: &mut T) -> Result<u32> {
    let first_byte = buf.read_u8()?;
    if first_byte != 0xFF {
        return Ok(first_byte.into());
    }
    buf.read_u32::<LittleEndian>()
}

pub fn deserialize_string<T: Read>(buf: &mut T) -> Result<String> {
    let empty = deserialize_bool(buf)?;
    if !empty {
        let len = deserialize_space_optimized_uint(buf)?;
        let mut string_buffer = vec![0u8; len as usize];
        buf.read_exact(&mut string_buffer)?;

        Ok(String::from_utf8(string_buffer).unwrap())
    } else {
        Ok(String::new())
    }
}


fn deserialize_collection<T: Read>(buf: &mut T, empty_names: bool) -> Result<Dictionary> {
    let len = deserialize_uint(buf)?;
    let mut values = vec![];

    for _ in 0..len {
        let name = deserialize_string(buf)?;

        if empty_names && name.len() != 0 {
            panic!("A list must not contain elements with names");
        } else if !empty_names && name.len() == 0 {
            panic!("A dictionary must not contain elements without names");
        }

        let value = deserialize_property_tree(buf)?;

        values.push(DictionaryValue { name, value });
    }

    Ok(Dictionary { values })
}

pub fn deserialize_list<T: Read>(buf: &mut T) -> Result<Dictionary> {
    deserialize_collection(buf, false)
}

pub fn deserialize_dictionary<T: Read>(buf: &mut T) -> Result<Dictionary> {
    deserialize_collection(buf, false)
}

pub fn deserialize_property_tree<T: Read>(buf: &mut T) -> Result<PropertyTree> {
    let typ = deserialize_byte(buf)?;
    let any_type = deserialize_bool(buf)?;

    let value = match typ {
        0 => PropertyTreeValue::None,
        1 => PropertyTreeValue::Bool(deserialize_bool(buf)?),
        2 => PropertyTreeValue::Number(deserialize_number(buf)?),
        3 => PropertyTreeValue::String(deserialize_string(buf)?),
        4 => PropertyTreeValue::List(deserialize_list(buf)?),
        5 => PropertyTreeValue::Dictionary(deserialize_dictionary(buf)?),
        _ => panic!("Unknown property tree type {}", typ)
    };

    Ok(PropertyTree {
        any_type,
        value
    })
}





pub fn deserialize_binary_version<T: Read>(buf: &mut T) -> Result<ServerVersion> {
    let major = buf.read_u16::<LittleEndian>()?;
    let minor = buf.read_u16::<LittleEndian>()?;
    let patch = buf.read_u16::<LittleEndian>()?;
    let _dev  = buf.read_u16::<LittleEndian>()?;

    Ok(ServerVersion::new(major, minor, patch))
}
