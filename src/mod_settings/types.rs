use std::io::Write;


#[derive(Debug)]
pub struct DictionaryValue {
    pub(super) name: String,
    pub(super) value: PropertyTree
}

#[derive(Debug)]
pub struct Dictionary {
    pub(super) values: Vec<DictionaryValue>
}

#[derive(Debug)]
pub enum PropertyTreeValue {
    None,
    Bool(bool),
    Number(f64),
    String(String),
    List(Dictionary),
    Dictionary(Dictionary)
}

#[derive(Debug)]
pub struct PropertyTree {
    pub(super) any_type: bool,
    pub(super) value: PropertyTreeValue
}



impl PropertyTree {
    pub fn print_indented<T: Write>(&self, out: &mut T, spaces: usize, incr: usize) {
        self.value.print_indented(out, spaces, incr);
    }
}

impl Dictionary {
    pub fn print_indented<T: Write>(&self, out: &mut T, spaces: usize, incr: usize) {
        self.values.iter().for_each(|e| e.print_indented(out, spaces, incr));
    }
}

impl DictionaryValue {
    pub fn print_indented<T: Write>(&self, out: &mut T, spaces: usize, incr: usize) {
        let indent = " ".repeat(spaces);
        writeln!(out, "{}{}", indent, self.name).unwrap();
        self.value.print_indented(out, spaces+incr, incr);
    }
}

impl PropertyTreeValue {
    pub fn print_indented<T: Write>(&self, out: &mut T, spaces: usize, incr: usize) {
        let indent = " ".repeat(spaces);
        match self {
            PropertyTreeValue::None => writeln!(out, "{}---", indent).unwrap(),
            PropertyTreeValue::Bool(b) => writeln!(out, "{}{}", indent, b).unwrap(),
            PropertyTreeValue::Number(n) => writeln!(out, "{}{}", indent, n).unwrap(),
            PropertyTreeValue::String(s) => writeln!(out, "{}{}", indent, s).unwrap(),
            PropertyTreeValue::List(d) => d.print_indented(out, spaces, incr),
            PropertyTreeValue::Dictionary(d) => d.print_indented(out, spaces, incr)
        }
    }
}
