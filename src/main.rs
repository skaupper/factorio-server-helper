#[macro_use]
extern crate clap;

mod validators;
mod cli;
mod subcommands;
mod mods;
mod mod_settings;
mod utils;

use std::collections::HashMap;
use clap::ArgMatches;




fn main() {
    let matches = cli::parse_args().get_matches();

    let subcommand_lookup: HashMap<_, _> = [
        ("init", subcommands::init as fn(&ArgMatches, &str)),
        ("create_save", subcommands::create_save),
        ("download_server", subcommands::download_server),
        ("generate_map", subcommands::generate_map),
        ("generate_modpack", subcommands::generate_modpack),
        ("update_modpack", subcommands::update_modpack),
        ("dump_mod_settings", subcommands::dump_mod_settings),
        ("start", subcommands::start)
    ]
    .iter()
    .cloned()
    .collect();

    let config_file = matches
        .value_of("CONFIG_FILE")
        .expect("The config file should have a default");
    let config_file = shellexpand::full(config_file).unwrap();

    match matches.subcommand() {
        // an empty subcommand names means that non was specified
        ("", _) => panic!("No subcommand should not be possible!"),
        (name, Some(args)) => subcommand_lookup[name](args, &config_file),
        _ => panic!("Nothing matched!"),
    }
}
