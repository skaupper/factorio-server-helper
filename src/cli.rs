use crate::validators::{valid_version, existing_file, non_empty};


pub fn parse_args<'a, 'b>() -> clap::App<'a, 'b> {
    clap_app!(factorio_server_helper =>
        (author: "Sebastian Kaupper <kauppersebastian@gmail.com>")
        (about: "Helps managing factorio servers and saves")
        (@arg CONFIG_FILE: -c --config +takes_value default_value("~/.factorio-server") "Sets a custom config file")
        (@subcommand init =>
            (about: "Initializes the root directory and creates a default config")
            (@arg ROOT_DIR: default_value("~/factorio-server/") "Sets a custom config file")
            (@arg USERNAME: -u --username +takes_value +required {non_empty} "Username to be used to log into the mod portal")
            (@arg PASSWORD: -p --password +takes_value +required {non_empty} "Password to be used to log into the mod portal")
        )
        (@subcommand create_save =>
            (about: "Creates a default save without generating a map")
            (@arg SAVE_NAME: default_value("default") {non_empty} "Sets a custom config file")
            (@arg SERVER_VERSION: -v --("server-version") default_value("latest") {non_empty} {valid_version} "Server version for the default map and server settings")
        )
        (@subcommand generate_map =>
            (about: "Generates the map for an existing save")
            (@arg SAVE_NAME: default_value("default") {non_empty} "Name of the save the map gets generated for")
            (@arg SERVER_VERSION: -v --("server-version") default_value("latest") {non_empty} {valid_version} "Server version the map gets generated with")
        )
        (@subcommand download_server =>
            (about: "Download a specific version of the headless server")
            (@arg SERVER_VERSION: -v --("server-version") default_value("latest") {non_empty} {valid_version} "Server version to be downloaded")
        )
        (@subcommand generate_modpack =>
            (about: "Download and save a bunch of mods into a separate mod directory")
            (@arg MOD_PACK: -m --("mod-pack") default_value("default") {non_empty} "Directory in which the mods should be stored (relative to mod_dir config)")
            (@arg MOD_LIST: -l --("mod-list") +takes_value {non_empty} {existing_file} "mod-list.json from which the mod names get extracted")
            (@arg MOD_NAME: +multiple {non_empty} "Name of a mod to be downloaded")
        )
        (@subcommand update_modpack =>
            (about: "Updates an already existing mod pack to the newest version of each mod")
            (@arg MOD_PACK: -m --("mod-pack") default_value("default") {non_empty} "Name of the mod pack to be updated")
        )
        (@subcommand dump_mod_settings =>
            (about: "Deserializes the binary mod-settings.dat of the given mod pack and prints it to stdout")
            (@arg MOD_PACK: -m --("mod-pack") default_value("default") {non_empty} "Name of the mod pack whose mod settings should be dumped")
            (@arg OUTPUT_FILE: -o --("output") default_value("-") {non_empty} "Path of the file the dump should be stored in. - for stdout")
        )
        (@subcommand start =>
            (about: "Start an existing save")
            (@arg MOD_PACK: -m --("mod-pack") default_value("default") {non_empty} "Mod pack to be used")
            (@arg SAVE_NAME: default_value("default") {non_empty} "Save to be started")
            (@arg SERVER_VERSION: -v --("server-version") default_value("latest") {non_empty} {valid_version} "Server version the save gets started with")
        )
    )
}
